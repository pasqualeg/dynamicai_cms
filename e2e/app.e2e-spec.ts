import { DynamicaiPage } from './app.po';

describe('dynamicai App', () => {
  let page: DynamicaiPage;

  beforeEach(() => {
    page = new DynamicaiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
