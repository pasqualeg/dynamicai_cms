import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {POIService} from '../poi.service';
import {POI} from '../models/poi';
import { Location } from '@angular/common';
import {Router} from '@angular/router';
import {Locat} from '../models/loc';

@Component({
  selector: 'app-poi',
  templateUrl: './poi.component.html'
})
export class POIComponent implements OnInit {
  id:string;
  isNew:boolean=false;
  isHidden:boolean=false;
  isIntendInput:boolean=false;
  isEditIntend:boolean=false;
  selectedIntend:string="";
  poi:POI;

  constructor(
  private poiService: POIService,
  private route: ActivatedRoute,
  private router:Router,
  private location: Location
  ) {}


  ngOnInit():void {
    this.getPOI();
  }

  changeIsHidden():void{
    if(this.isHidden)
      this.isHidden=false;
    else
      this.isHidden=true;
  }

  getPOI():void{
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
   if(!this.id){
     this.poi=new POI();
     this.id="";
     this.isNew=true;
   }else{
     this.poiService.getPOI(this.id).subscribe(poi =>
      {
       this.poi=poi;
     });
   }
  }

  editPOI(poi):void{
    this.poiService.editPOI(this.poi);
    this.changeIsHidden();
  }

  addPOI(poi):void{
    this.poiService.addPOI(this.poi).then(addedPoi=>{
      console.log(addedPoi);
      this.poi=addedPoi;
      this.isHidden=false;
      this.isNew=false;
    });
    // this.router.navigate(['/']);
  }

  addNewIntend(event,text:string):void{
    if(event.key=="Enter"){
      if(this.poi.tags)
        this.poi.tags.push(text);
      else{
        this.poi.tags=[text];
      }
      this.poiService.editPOI(this.poi);
    }
  }

  editIntend(event,text:string,selected:string):void{
    if(event.key=="Enter"){
      this.poi.tags[this.poi.tags.indexOf(selected)]=text;
      this.poiService.editPOI(this.poi);
      this.isEditIntend=false;
    }
  }

  changeIntendInput():void{
    if(this.isIntendInput)
      this.isIntendInput=false;
    else
      this.isIntendInput=true;
  }

  goToEditIntend(text:string):void{
    this.selectedIntend=text;
    this.isEditIntend=true;
  }

  changeIsIntendEdit(){
    this.isEditIntend?this.isEditIntend=false:this.isEditIntend=true;
  }

  deleteIntend(intend:string):void{
    this.poi.tags=this.poi.tags.filter(i=>i!=intend);
    this.poiService.editPOI(this.poi);
  }
}
