import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { AppRoutingModule }     from './app-routing.module';
import { AppComponent } from './app.component';
import { ListPOIComponent } from './list-poi/list-poi.component';
import {POIService} from './poi.service';
import { POIComponent } from './poi/poi.component';
import { IntendComponent } from './intend/intend.component';


@NgModule({
  declarations: [
    AppComponent,
    ListPOIComponent,
    POIComponent,
    IntendComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [POIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
