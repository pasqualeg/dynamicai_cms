import {Locat} from './loc';
export class POI{
  id:string;
  name:string;
  tags:string[];
  rating:number;
  description:string;
  location:Locat;


  constructor(){
    this.id="";
    this.name="";
    this.tags=[];
    this.rating=0;
    this.description="";
    this.location=new Locat();
  }
}
