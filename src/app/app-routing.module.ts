import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { POIComponent } from './poi/poi.component';
import {ListPOIComponent} from './list-poi/list-poi.component';

const routes: Routes=[
    {path:'',component:ListPOIComponent},
    {
      path: 'poi/:id',
      component: POIComponent
    },
    {
      path: 'poi',
      component: POIComponent
    }

  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
