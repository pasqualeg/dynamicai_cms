import { Injectable } from '@angular/core';
import {Http, Response,RequestOptions ,Headers} from '@angular/http';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { ActivatedRoute, ParamMap } from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {POI} from './models/poi';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

@Injectable()

export class POIService {

  constructor(private http:Http) { }
  private url="http://91.205.175.202:42356/pois/";
  headers = new Headers({ 'Content-Type': 'application/json' });
  options = new RequestOptions({ headers: this.headers });

  getPOIs(){
    return  this.http.get(this.url).map((response: Response) => response.json() as POI[]);
  }

  deletePOI(poi):Promise<void>{
    return this.http.delete(this.url+poi.id, {headers: this.headers})
    .toPromise()
    .then(() => null)
    .catch(this.handleError);
  }

  getPOI(id:string){
    return  this.http.get(this.url+id).map((response: Response) => response.json() as POI);
  }

  addPOI(poi:any):Promise<any>{
      const url = this.url;
      return this.http
        .post(url, JSON.stringify(poi),this.options)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    editPOI(poi:any):Promise<any>{
      const url = this.url+poi.id;
      return this.http
        .put(url, JSON.stringify(poi),this.options)
        .toPromise()
        .then(() => poi)
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }



}
