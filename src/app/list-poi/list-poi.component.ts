import { Component, OnInit } from '@angular/core';
import {POIService} from '../poi.service';
import {POI} from '../models/poi';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-poi',
  templateUrl: './list-poi.component.html'
})

export class ListPOIComponent implements OnInit {

  POIs:POI[] ;

  constructor(private poiService:POIService,private router:Router) { }

  getPOIs(){
    this.poiService.getPOIs().subscribe(POIs=>{
      this.POIs=POIs as POI[];
    });
  }

  delete(poi):void{
    this.poiService.deletePOI(poi).then(()=>{
      this.POIs=this.POIs.filter(p=>p !== poi);
    });
  }

  editPOI(id:string):void{
    this.router.navigate(['/poi/'+id]);
  }


  ngOnInit():void{
    this.getPOIs();
  }

}
